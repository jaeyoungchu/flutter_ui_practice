import 'package:flutter/material.dart';
import 'screens/EasingAnimationWidget.dart';
import 'screens/home.dart';
import 'screens/snackBarScreen.dart';
import 'screens/TabBarScreen.dart';
import 'screens/fontScreen.dart';
import 'screens/drawerScreen.dart';
import 'screens/orientationScreen.dart';
import 'screens/imageScreen.dart';
import 'screens/differentListScreen.dart';
import 'screens/gestureScreen.dart';
import 'screens/rippleScreen.dart';
import 'screens/dismissItem.dart';
import 'screens/passingDateScreen.dart';
import 'screens/returnValueScreen.dart';
import 'screens/heroAnimationScreen.dart';
import 'screens/fadeInOutScreen.dart';
import 'screens/fetchDataScreen.dart';
import 'screens/preferenceScreen.dart';
import 'screens/formStateScreen.dart';
import 'screens/retrieveTextFieldScreen.dart';
import 'screens/layout1Screen.dart';

void main() => runApp(new BaseMaterialApp());


class BaseMaterialApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Practice Material',
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.lightBlue[800],
        accentColor: Colors.cyan[600],
      ),
      routes: <String,WidgetBuilder>{
        '/homeScreen' : (BuildContext context) => new home(),
        '/snackBarScreen' : (BuildContext context) => new SnackBarScreen(),
        '/TabBarScreen' : (BuildContext context) => new TabBarScreen(),
        '/fontsScreen' : (BuildContext context) => new fontScreen(),
        '/drawerScreen' : (BuildContext context) => new drawerScreen(title:'Drawer Demo'),
        '/orientationScreen' : (BuildContext context) => new orientationScreen(),
        '/imageScreen' : (BuildContext context) => new imageScreen(),
        '/differentListScreen' : (BuildContext context) => new differentScreen(),
        '/gestureScreen' : (BuildContext context) => new gestureScreen(),
        '/rippleScreen' : (BuildContext context) => new rippleScreen(),
        '/dismissItemScreen' : (BuildContext context) => new dismissItem(),
        '/todosScreen' : (BuildContext context) => new todosScreen(),
        '/returnValueScreen' : (BuildContext context) => new returnValueScreen(),
        '/heroAnimationScreen' : (BuildContext context) => new heroAnimationScreen(),
        '/fadeInOutScreen' : (BuildContext context) => new fadeInOutScreen(),
        '/fetchDataScreen' : (BuildContext context) => new fetchDataScreen(),
        '/preferenceScreen' : (BuildContext context) => new preferenceScreen(),
        '/formStateScreen' : (BuildContext context) => new formStateScreen(),
        '/retrieveTextFieldScreen' : (BuildContext context) => new retrieveTextFieldScreen(),
        '/layout1Screen' : (BuildContext context) => new layout1Screen(),
        '/EasingAnimationWidgetScreen' : (BuildContext context) => new EasingAnimationWidget(),
      },
      home: home(),
    );
  }
}


