import 'package:flutter/material.dart';

class rippleScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ripple Button Demo'),
      ),
      body: Center(child: CustomRippleButton()),
    );
  }
}


class CustomRippleButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      // When the user taps the button, show a snackbar
      onTap: () {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Tap'),
        ));
      },
      child: Container(
        padding: EdgeInsets.all(12.0),
        child: Text('Flat Button'),
      ),
    );
  }
}
