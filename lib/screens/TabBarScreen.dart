import 'package:flutter/material.dart';

class TabBarScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(tabs: [
            Tab(icon: Icon(Icons.directions_car),),
            Tab(icon: Icon(Icons.directions_transit),),
            Tab(icon: Icon(Icons.directions_bike),),
          ]),
          title: Text('Tabs Demo'),
        ),
        body: TabBarView(
            children: [
              getTabView1(),
              Icon(Icons.directions_transit),
              Icon(Icons.directions_bike),
            ]
        ),
      ),
    );
  }

  Widget getTabView1(){
    return Center(
      child: Container(
        padding: EdgeInsets.all(50.0),
        child: new Column(
          children: <Widget>[
            Icon(Icons.directions_car),
            Icon(Icons.directions_transit),
            Icon(Icons.directions_bike),
            Text('TabView1'),
          ],
        ),
      ),
    );
  }
}


