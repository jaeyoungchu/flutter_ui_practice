import 'package:flutter/material.dart';
import 'package:flutter_second_parctice/screens/home.dart';

class drawerScreen extends StatelessWidget {

  final String title;

  drawerScreen({Key key,this.title}) : super(key:key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title),),
      body: Center(
        child: Text('Drawer Page'),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text('Drawer header'),
              decoration: BoxDecoration(color: Colors.blue),
            ),
            ListTile(
              title: Text('Item 1'),
              onTap: (){
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('Go to Home'),
              onTap: (){
                Navigator.pop(context);

//                Navigator.of(context).pushNamed('/homeScreen');

//                Navigator.push(
//                  context,
//                  new MaterialPageRoute(builder: (context)=> new home())
//                );

                Navigator.of(context).pushNamedAndRemoveUntil('/homeScreen',(Route<dynamic> route) => false);
              },
            ),
          ],
        ),
      ),
    );
  }
}
