import 'package:flutter/material.dart';

class SnackBarScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('SnackBarScreen'),),
        body: new Builder(
            builder: (BuildContext context){
              return Center(
                child: RaisedButton(
                  child: Text('Make Snackbar'),
                  onPressed: (){
                    final snackBar= SnackBar(
                      content: Text('Yes Snack Bar!!'),
                      action: SnackBarAction(
                        label: 'Undo',
                        onPressed: (){
                          print('label clicked');
                        },
                      ),

                    );
                    Scaffold.of(context).showSnackBar(snackBar);
                  },
                ),
              );
            }
        ),

    );
  }


}
