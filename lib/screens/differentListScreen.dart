import 'package:flutter/material.dart';

class differentScreen extends StatelessWidget {

  List<ListItem> items;


  differentScreen(){
    items = List<ListItem>.generate(100, (i){
     return i % 6 == 0 ? HeadingItem("Heading $i") : MessageItem("Sender $i", "message body $i");
    });
  }

  @override
  Widget build(BuildContext context) {
    print(items.length);
    return Scaffold(
      appBar: AppBar(title: Text('Mixed List'),),
      body: ListView.builder(
          itemBuilder: (context,index){
            final item = items.elementAt(index);

            if(item is HeadingItem){

              return ListTile(
                title: Text(item.heading, style: Theme.of(context).textTheme.headline,),
              );
            }else if(item is MessageItem){
              return ListTile(
                title: Text(item.sender),
                subtitle: Text(item.body),
              );
            }
          },
        itemCount: items.length,
      ),
    );
  }
}


// The base class for the different types of items the List can contain
abstract class ListItem {}

// A ListItem that contains data to display a heading
class HeadingItem implements ListItem {
  final String heading;

  HeadingItem(this.heading);
}

// A ListItem that contains data to display a message
class MessageItem implements ListItem {
  final String sender;
  final String body;

  MessageItem(this.sender, this.body);
}