import 'package:flutter/material.dart';

class retrieveTextFieldScreen extends StatefulWidget {
  @override
  _retrieveTextFieldScreenState createState() => _retrieveTextFieldScreenState();
}

class _retrieveTextFieldScreenState extends State<retrieveTextFieldScreen> {

  final myController = TextEditingController();
  final secondController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    secondController.addListener(_printValue);
  }

  void _printValue(){
    print('text field:${secondController.text}');
  }

  @override
  void dispose() {
    myController.dispose();
    secondController.removeListener(_printValue);
    secondController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Retrieve Text Input'),
      ),
      body: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              TextField(
                controller: myController,
              ),
              TextField(
                controller: secondController,
              ),
            ],
          ),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: (){
            showDialog(
                context: context,
                builder: (context){
                  return AlertDialog(
                    content: Text(myController.text),
                  );
                }
            );
          },
          tooltip: 'Show me the value!',
          child: Icon(Icons.text_fields),
      ),



    );
  }
}
