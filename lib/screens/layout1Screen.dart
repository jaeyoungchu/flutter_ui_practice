import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class layout1Screen extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(title: Text('Layout1'),),
      body: Container(
        padding: EdgeInsets.zero,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: screenSize.height*0.44,
              child: CachedNetworkImage(
                imageUrl: 'https://github.com/flutter/website/blob/master/src/_includes/code/layout/lakes/images/lake.jpg?raw=true',
                placeholder: Center(child: CircularProgressIndicator()),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Container(
              padding: EdgeInsets.all(6.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 14.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('asfuhwuiofhio',style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),textAlign: TextAlign.start,),
                        Text('asfqwfqeefuhwuiofhio',style: TextStyle(fontWeight: FontWeight.normal, fontSize: 16.0),textAlign: TextAlign.start,),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 20.0),
                    child: Row(
                        children: <Widget>[
                          Icon(Icons.star,color: Colors.red,),
                          Text('41',)
                        ],
                     ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Icon(Icons.call,color: Colors.lightBlue,),
                    Text('CALL',style: TextStyle(color: Colors.lightBlue),)
                  ],
                ),
                Column(
                  children: <Widget>[
                    Icon(Icons.near_me,color: Colors.lightBlue,),
                    Text('ROUTE',style: TextStyle(color: Colors.lightBlue),)
                  ],
                ),
                Column(
                  children: <Widget>[
                    Icon(Icons.share,color: Colors.lightBlue,),
                    Text('SHARE',style: TextStyle(color: Colors.lightBlue),)
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(14.0),
                child: SingleChildScrollView(
                  child: Text('''
Lake Oeschinen lies at the foot of the Blüemlisalp in the Bernese Alps. Situated 1,578 meters above sea level, it is one of the larger Alpine Lakes. A gondola ride from Kandersteg, followed by a half-hour walk through pastures and pine forest, leads you to the lake, which warms to 20 degrees Celsius in the summer. Activities enjoyed here include rowing, and riding the summer toboggan run.
        ''',
                    softWrap: true,),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
