import 'package:flutter/material.dart';

class orientationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Orientation Demo'),),
      body: OrientationBuilder(
          builder: (context, orientation){
            return GridView.count(
              crossAxisCount: orientation == Orientation.portrait ? 2:3,
              shrinkWrap: true,
              childAspectRatio: (15/5),
              children: List.generate(100, (index){
                return Card(
                  child: Container(
                    child: Center(
                      child: Text('Item $index',style: Theme.of(context).textTheme.headline,),
                    ),
                  ),
                );
              }),
            );
          }
      ),
    );
  }
}
