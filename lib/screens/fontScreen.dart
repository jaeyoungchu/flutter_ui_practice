import 'package:flutter/material.dart';

class fontScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Custom Fonts',style: TextStyle(fontSize: 27.0,fontFamily: 'Mukta',fontWeight: FontWeight.bold),),),
      body: Center(
        child : Container(
          padding: EdgeInsets.all(20.0),
          child: new Column(
            children: <Widget>[
              Text('12345678 fonts is working',style: TextStyle(fontSize: 22.0),),
              Text('12345678 fonts is working',style: TextStyle(fontFamily: 'Raleway',fontSize: 22.0),),
            ],
          ),
        )
      ),
    );
  }
}
