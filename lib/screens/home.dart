import 'package:flutter/material.dart';

class home extends StatelessWidget {

  List<String> _practiceNameList;


  home(){
    _practiceNameList = new List();
    _practiceNameList.add('snackBar');
    _practiceNameList.add('TabBar');
    _practiceNameList.add('fonts');
    _practiceNameList.add('drawer');
    _practiceNameList.add('orientation');
    _practiceNameList.add('image');
    _practiceNameList.add('differentList');
    _practiceNameList.add('gesture');
    _practiceNameList.add('ripple');
    _practiceNameList.add('dismissItem');
    _practiceNameList.add('todos');
    _practiceNameList.add('returnValue');
    _practiceNameList.add('heroAnimation');
    _practiceNameList.add('fadeInOut');
    _practiceNameList.add('fetchData');
    _practiceNameList.add('preference');
    _practiceNameList.add('formState');
    _practiceNameList.add('retrieveTextField');
    _practiceNameList.add('layout1');
    _practiceNameList.add('EasingAnimationWidget');
  }

  void onTapItem(BuildContext context,int value){
    String route = '/${_practiceNameList[value]}Screen';
    print(route);
    Navigator.of(context).pushNamed(route);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: Text('Home'),),
      body: OrientationBuilder(
          builder: (context, orientation){
            return GridView.count(
              crossAxisCount: 2,
              childAspectRatio: (15/5),
              children: List.generate(_practiceNameList.length, (index){
                return GestureDetector(
                  onTap: (){onTapItem(context,index);},
                  child: Container(
                    child: Card(
                        child: Center(
                          child: Text('${_practiceNameList[index]}',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 22.0),),
                        ),
                    ),
                  ),
                );
              }),
            );
          }
      ),
    );
  }
}
